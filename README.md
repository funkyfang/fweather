#Fweather
Fweather is based on [Tweather](https://github.com/twitterdev/tweather).

Using [Twitter](https://twitter.com/), get weather forecasts or traffic for your current location by mentioning [@Fweather](https://twitter.com/fweather1234).

For weather,

* Tweet text containing an address or a place (e.g., 94107).

For traffic,

* Tweet the word "traffic" and an address (e.g., traffic 94107).

![An example weather screenshot.](weather_example.png)

![An example traffic screenshot.](traffic_example.png)

#Requirements
* [Node.js](https://nodejs.org/)
* [OpenWeatherMaps API](http://openweathermap.org/current) for *weather forecast data* (**requires** an [app id](http://openweathermap.org/appid))
* [Google Maps API](https://developers.google.com/maps/documentation/geocoding/intro) for *geocoding*
* [Weather Underground API](https://www.wunderground.com/weather/api/d/docs?d=layers/radar) for *weather radar images* (**requires** a [key id](https://www.wunderground.com/weather/api/))
* [PageSpeed API](https://developers.google.com/speed/docs/insights/v2/reference/pagespeedapi/runpagespeed) for *getting screenshots* of Google Maps.

#Getting Started
1. Register a [Twitter application](https://apps.twitter.com).
2. Run `git clone https://funkyfang@bitbucket.org/funkyfang/fweather.git`.
3. Run `npm install`.
4. Run `cp app/config.sample.js app/config.js`.
5. Replace the empty strings `''` in `app/config.js` with your values.
6. Run `node app/app.js`.