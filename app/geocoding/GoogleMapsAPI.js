var request = require('request')
var qs = require('querystring')

var url = 'https://maps.googleapis.com/maps/api/geocode/json?'

module.exports.geocodeAddress = function(value, callback) {
  var query = qs.stringify({ address: value })

  request(url + query, function(err, response, body) {
    var json = JSON.parse(body)
    if (json && json.results.length > 0) {
      var result = json.results[0] 
      callback({
          lat     : result.geometry.location.lat
        , lon     : result.geometry.location.lng
        , address : result.formatted_address
      })
    } else {
      console.log('GoogleMapsAPI error getting: ' + url + query)
    }
  })
}
