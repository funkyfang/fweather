var GoogleMapsAPI = require('../geocoding/GoogleMapsAPI.js')
var qs = require('querystring')
var request = require('request')
var util = require('util')

var url = 'https://www.googleapis.com/pagespeedonline/v2/runPagespeed?'

function get_maps_url(lat, lon) {
    var url = 'https://www.google.com/maps/@%d,%d,%s/data=%s'
    var zoom = '15z'
    var data = '!5m1!1e1'
    return util.format(url, lat, lon, zoom, data)
}

function get_status(tweet, address, base64_img) {
  var name = '@' + tweet.user.screen_name
  if (base64_img) {
    return name + ', the current traffic for ' + address + '.'
  } else {
    return name + " sorry, we couldn't deterine the traffic for that address :(." +
        ' Please try again!'
  }
}

module.exports.get = function(tweet, address, callback) {
  GoogleMapsAPI.geocodeAddress(address, function(result) {
    var query = qs.stringify({
        url        : get_maps_url(result.lat, result.lon)
      , screenshot : 'true'
      , strategy   : 'mobile'
    })

    request(url + query, function(err, resp, body) {
      if (body) {
        var json = JSON.parse(body)
        var base64_img = json.screenshot.data
        var status = get_status(tweet, result.address, base64_img)
        callback(status, base64_img)
      } else {
        console.log('TrafficImg error getting: ' + url + query)
      }
    })
  })
}
