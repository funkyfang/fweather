var config = require('./config')
var TrafficImg = require('./traffic/TrafficImg.js')
var Twitter = require('twit')
var Weather = require('./weather/Weather.js')

var twitter = new Twitter(config.twitter)

// Stream of @mentions for weather_account
var stream = twitter.stream('statuses/filter', { track: config.twitter.weather_account })

// Attempting connection to streaming API
stream.on('connect', function (response) {
  console.log("Opening Twitter streaming connection.")
})

// Connection opened
stream.on("connected", function (response) {
  console.log("Streaming...")
})

// The @mentions
stream.on('tweet', function (tweet) {
  try {
    handleTweet(tweet)
  } catch (err) {
    console.log("Unexpected error handling tweet: ", tweet)
  }
})

function handleTweet(tweet) {
  var app = null
  var words = strip_mentions(tweet.text)
  var address = words.join(' ')

  switch (words[0].toLowerCase()) {
    case 'traffic':
      words.shift()
      address = words.join(' ')
      app = TrafficImg
      break
    default:
      app = Weather
  }

  // Basic interface check.
  if (typeof app.get === "function") {
    app.get(tweet, address, function(status, base64_img) {
      respond(tweet, status, base64_img)
    })
  } else {
    console.log("App must implement a 'get' function which returns a 'status'" +
      " string and an optional 'base64_img' base64 encoded image.")
  }
}

function strip_mentions(str) {
  var words = str.split(' ')
  var no_mentions = []
  for (i=0; i<words.length; i++) {
    if (words[i].charAt(0) != '@') {
      no_mentions.push(words[i])
    }
  }
  return no_mentions
}

function respond(tweet, status, base64_img) {
  var params = {
      status                : status
    , in_reply_to_status_id : tweet.id_str
  }
  twitter.post('media/upload', { media: base64_img }, function (err, data, resp) {
    if (err) {
      console.log('Media failed to upload.')
    } else if (data) {
      var mediaIdStr = data.media_id_string
      params['media_ids'] = [mediaIdStr]
    }
    twitter.post('statuses/update', params, function (err, data, resp) {
      if (err) {
        console.log(err)
      } else {
        console.log(data)
      }
    })
  })
}
