var GoogleMapsAPI = require('../geocoding/GoogleMapsAPI.js')
var OpenWeatherMapAPI = require('../weather/OpenWeatherMapAPI.js')

function get_status(tweet, address, weather) {
  var name = '@' + tweet.user.screen_name
  switch (weather.cod) {
    case 200:
      var place = weather.name
      if (address) {
        place = address
      }
      var high_temp = Math.round(weather.main.temp_max)
      var low_temp = Math.round(weather.main.temp_min)
      return name + ", today's forecast for " + place +
        ' is ' + weather.weather[0].main.toLowerCase() +
        ' with a high of ' + high_temp + '°F  and a low of ' + low_temp + '°F.'
      break
    case 404:
      return name + " sorry, we couldn't find a weather forecast that address :(." +
        ' Please try again!'
      break
    default:
      return name + " sorry, we can't geocode your tweet. Try tweeting an address " +
        'or geo-tagging your tweet.'
  }
}

module.exports.get = function(tweet, address, callback) {
  GoogleMapsAPI.geocodeAddress(address, function(result) {
    var lat = result.lat
    var lon = result.lon
    var address = result.address
    if (!lat || !lon) {
      var geo = tweet.coordinates
      if (geo && geo.type == 'Point') {
        lat = geo.coordinates[1]
        lon = geo.coordinates[0]
      }
    }

    OpenWeatherMapAPI.getWeather(lat, lon, function(weather, base64_gif) {
      var status = get_status(tweet, result.address, weather)
      callback(status, base64_gif)
    })
  })
}
