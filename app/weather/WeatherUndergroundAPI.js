var config = require('../config')
var qs = require('querystring')
var request = require('request').defaults({ encoding: null })

var url = 'http://api.wunderground.com/api/' +
  config.weather_underground_key_id + '/radar/image.gif?'

module.exports.getRadarImg = function(lat, lon, callback) {
  var query = qs.stringify({
      centerlat : lat
    , centerlon : lon
    , radius    : 20
    , newmaps   : 1
  })

  request.get(url + query, function (error, response, body) {
    if (body) {
      var base64_gif = new Buffer(body).toString('base64')
      callback(base64_gif)
    } else {
      console.log('WeatherUndergroundAPI error getting: ' + url + query)
    }
  })
}
