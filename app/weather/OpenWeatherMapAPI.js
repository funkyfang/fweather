var config = require('../config')
var qs = require('querystring')
var request = require('request')
var WeatherUndergroundAPI = require('./WeatherUndergroundAPI')

var url = 'http://api.openweathermap.org/data/2.5/weather?'

module.exports.getWeather = function(lat_val, lon_val, callback) {
  query = qs.stringify({
      appid : config.open_weather_app_id
    // The OpenWeatherMap API is sensitive to the precision of lat, lon.
    , lat   : lat_val.toFixed(1)
    , lon   : lon_val.toFixed(1)
    , units : 'imperial'
  })

  request(url + query, function (err, response, body) {
    if (body) {
      var weather = JSON.parse(body)

      WeatherUndergroundAPI.getRadarImg(lat_val, lon_val, function(gif) {
        callback(weather, gif)
      })
    } else {
      console.log("OpenWeatherMapAPI error getting: " +  url + query)
    }
  })
}
