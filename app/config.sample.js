module.exports.twitter = {
    weather_account     : ''
  , consumer_key        : ''
  , consumer_secret     : ''
  , access_token        : ''
  , access_token_secret : ''
}

module.exports.open_weather_app_id = ''

module.exports.weather_underground_key_id = ''
